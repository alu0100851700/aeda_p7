#pragma once
#include <cstdio>
using namespace std;

template <class clave>
class nodoAVL{
private:
	clave dato_;
	nodoAVL<clave>*	i_;	
	nodoAVL<clave>*	d_;	
	int bal_;

public:
	nodoAVL(const clave& dato, const int& bal):
		dato_(dato),
		i_(NULL),
		d_(NULL),
		bal_(bal){}

	nodoAVL(void){
		dato_ = new clave();
		i_ = NULL;
		d_ = NULL;
		bal_ = 0;
	}

	~nodoAVL(void){}

	//GET METHODS
	clave get_dato(void) const{
		return dato_;
	}

	nodoAVL<clave>*& get_i(void){
		return i_;
	}

	nodoAVL<clave>*& get_d(void){
		return d_;
	}

	int get_bal(void){
		return bal_;
	}

	//SET METHODS
	void set_dato(const clave &dato){
		dato_ = dato;
	}

	void set_i(nodoAVL<clave>* i){
		i_ = i;
	}

	void set_d(nodoAVL<clave>* d){
		d_ = d;
	}

	void set_bal(const int& bal){
		bal_ = bal;
	}

};