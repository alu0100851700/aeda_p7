#pragma once 
#include <cstdio>
#include <cstdlib>
#include <time.h>
#include <iostream>

using namespace std;

class dni{
private:
	int num_;


public:
	dni(){
		srand (rand());
		num_ = 30000000 + rand()%50000000;
	}

	dni(const int& num):
		num_(num){}

	dni(const dni& d){
		num_ = d.get_num();
	}

	~dni(void){}

	/////////////////////////////
	// METODOS DE ACCESO
	/////////////////////////////

	inline int get_num(void) const{
		return num_;
	}

	inline void set_num(const int& num){
		num_ = num;
	}

	/////////////////////////////
	// SOBRECARGA DE OPERADORES
	/////////////////////////////
	bool operator <(dni& dni2){
		return num_ < dni2.get_num();
	}

	bool operator <=(dni& dni2) {
		return num_ <= dni2.get_num();
	}

	bool operator >(dni& dni2){
		return num_ > dni2.get_num();
	}

	bool operator >=(dni& dni2){
		return num_ >= dni2.get_num();
	}

	bool operator ==(dni& dni2){
		return num_ == dni2.get_num();
	}

	bool operator !=(dni& dni2){
		return num_ != dni2.get_num();
	}

	inline int operator %(int a) const{
		return (num_%a);
	}

	// Operadores de casteo
	inline operator int() const
    {
        return num_;
    }

	friend ostream& operator <<(ostream& os, const dni& d);

};

ostream& operator <<(ostream& os, const dni& d){
	os << d.get_num();
	return os;
}