#pragma once
#include <iostream>
#include <cstdio>
#include <cmath>
#include <queue>
#include "nodoAVL.hpp"
using namespace std;

template<class clave>
class arbolAVL{
private:
	nodoAVL<clave>* raiz_;
	int nNodos;

	//Modo estadistica
	int nCompB;	//Numero de comparaciones en la busqueda
	int nCompI;	//Numero de comparaciones en la inserción

public:

	arbolAVL():
		raiz_(NULL),
		nNodos(0),
		nCompB(0),
		nCompI(0){}

	~arbolAVL(){
		BorrarArbol(raiz_);
	}

	void BorrarArbol(nodoAVL<clave>* &raiz){
		if(raiz -> get_i() != NULL )
			BorrarArbol(raiz -> get_i());
		if(raiz -> get_d() != NULL )
			BorrarArbol(raiz -> get_d());
		delete raiz;
	}

	void rotacion_II(nodoAVL<clave>* &nodo){
		nodoAVL<clave>* nodo1 = nodo -> get_i();
		nodo -> set_i(nodo1->get_d());
		nodo1 -> set_d(nodo);

		if(nodo1->get_bal() == 1){
			nodo->set_bal(0);
			nodo1->set_bal(0);
		}
		else{
			nodo->set_bal(1);
			nodo1->set_bal(-1);
		}
		nodo = nodo1;
	}

	void rotacion_DD(nodoAVL<clave>* &nodo){
		nodoAVL<clave>* nodo1 = nodo -> get_d();

		nodo -> set_d(nodo1->get_i());
		nodo1 -> set_i(nodo);

		if(nodo1->get_bal() == -1){
			nodo->set_bal(0);
			nodo1->set_bal(0);
		}
		else{
			nodo->set_bal(-1);
			nodo1->set_bal(1);
		}
		nodo = nodo1;
	}

	void rotacion_ID(nodoAVL<clave>* &nodo){
		nodoAVL<clave>* nodo1 = nodo -> get_i();

		nodoAVL<clave>* nodo2 = nodo1 -> get_d();

		nodo -> set_i(nodo2->get_d());
		nodo2 -> set_d(nodo);

		nodo1 -> set_d(nodo2->get_i());
		nodo2 -> set_i(nodo1);

		if(nodo2->get_bal() == -1)	nodo1->set_bal(1);
		else						nodo1->set_bal(0);
		
		if(nodo2->get_bal() == 1)	nodo->set_bal(-1);
		else						nodo->set_bal(0);

		nodo2 -> set_bal(0);
		nodo = nodo2;
	}

	void rotacion_DI(nodoAVL<clave>* &nodo){
		nodoAVL<clave>* nodo1 = nodo -> get_d();

		nodoAVL<clave>* nodo2 = nodo1 -> get_i();

		nodo -> set_d(nodo2->get_i());
		nodo2 -> set_i(nodo);

		nodo1 -> set_i(nodo2->get_d());
		nodo2 -> set_d(nodo1);

		if(nodo2->get_bal() == 1)	nodo1->set_bal(-1);
		else						nodo1->set_bal(0);
		
		if(nodo2->get_bal() == -1)	nodo->set_bal(1);
		else						nodo->set_bal(0);

		nodo2 -> set_bal(0);
		nodo = nodo2;
	}

	void inserta(clave& dato){
		nodoAVL<clave>* nuevo = new nodoAVL<clave>(dato,0);
		bool crece = false;
		inserta_bal(raiz_, nuevo, crece);
	}

	void inserta_bal(nodoAVL<clave>* &nodo, nodoAVL<clave>* &nuevo, bool& crece){
		nCompI++;
		if(nodo == NULL){
			nodo = nuevo;
			crece = true;
		}
		else if(nuevo -> get_dato() < nodo -> get_dato()){
			inserta_bal(nodo->get_i(), nuevo, crece);
			if(crece) insert_re_balancea_izq(nodo,crece);
		}
		else{
			inserta_bal(nodo->get_d(), nuevo, crece);
			if(crece) insert_re_balancea_dcha(nodo,crece);
		}
	}

	void insert_re_balancea_izq(nodoAVL<clave>* &nodo, bool& crece){
		switch(nodo->get_bal()){
			case -1: 	nodo->set_bal(0);
						crece = false;
						break;

			case 0:		nodo->set_bal(1);
						break;

			case 1:		nodoAVL<clave>* nodo1 = nodo -> get_i();
						if(nodo1->get_bal() == 1)
							rotacion_II(nodo);
						else rotacion_ID(nodo);
						crece = false;
						break;

		}
	}

	void insert_re_balancea_dcha(nodoAVL<clave>* &nodo, bool& crece){
		switch(nodo->get_bal()){
			case 1:	nodo->set_bal(0);
						crece = false;
						break;

			case 0:		nodo->set_bal(-1);
						break;

			case -1:		
						nodoAVL<clave>* nodo1 = nodo -> get_d();
						if(nodo1->get_bal() == -1)
							rotacion_DD(nodo);
						else rotacion_DI(nodo);
						crece = false;
						break;

		}
	}

	nodoAVL<clave>* busca(const clave &dato){
		return buscaR(dato, raiz_);
	}

	nodoAVL<clave>* buscaR(const clave &dato, nodoAVL<clave>* &raiz){
		nCompB++;
		if(raiz == NULL)
			return NULL;
		if(raiz -> get_dato() == dato)	
			return raiz;
		if(dato < raiz -> get_dato())
			return buscaR(dato, raiz -> get_i());
		return buscaR(dato, raiz -> get_d());
	}

	void eliminar(const clave &dato){
		bool decrece = false;
		elimina_rama(raiz_, dato, decrece);
		nNodos--;
	}

	void elimina_rama(nodoAVL<clave>* &nodo, const clave &dato, bool& decrece){
		if(nodo == NULL) return;
		if(dato < nodo -> get_dato()){
			elimina_rama(nodo->get_i(),dato,decrece);
			if(decrece)
				elimina_re_balancea_izq(nodo,decrece);
		}
		else if(dato > nodo -> get_dato()){
			elimina_rama(nodo->get_d(),dato,decrece);
			if(decrece)
				elimina_re_balancea_dcha(nodo,decrece);
		}
		else{
			nodoAVL<clave>* Eliminado = nodo;
			if(nodo -> get_i() == NULL){
				nodo =  nodo -> get_d();
				decrece = true;
			}
			else if(nodo -> get_d() == NULL){
				nodo = nodo -> get_i();
				decrece = true;
			}
			else{
				sustituye(Eliminado, nodo->get_i(), decrece);
				if(decrece)
					elimina_re_balancea_izq(nodo,decrece);
			}

			delete Eliminado;
		}
	}

	void elimina_re_balancea_izq(nodoAVL<clave>* &nodo, bool& decrece){
		switch(nodo->get_bal()){
			case -1:
				{
					nodoAVL<clave>* nodo1 = nodo->get_d();
					if(nodo1->get_bal() > 0)
						rotacion_DI(nodo);
					else{
						if(nodo1->get_bal() == 0)
							decrece = false;
						rotacion_DD(nodo);
					}
				}
				break;

			case 0:		
				nodo->set_bal(-1);
				break;

			case 1:		
				nodo->set_bal(0);
				break;
		}
	}

	void elimina_re_balancea_dcha(nodoAVL<clave>* &nodo, bool& decrece){
		switch(nodo->get_bal()){
			case 1: 	
				{
					nodoAVL<clave>* nodo1 = nodo->get_i();
					if(nodo1 -> get_bal() < 0)
						rotacion_ID(nodo);
					else{
						if(nodo1->get_bal() == 0)
							decrece = false;
						rotacion_II(nodo);
					}
				}
				break;

			case 0:		nodo ->set_bal(1);
						break;

			case -1:
				nodo->set_bal(0);
				break;

		}
	}


	void sustituye(nodoAVL<clave>* &eliminado, nodoAVL<clave>* &sust, bool& decrece){
		if( sust->get_d() != NULL){
			sustituye(eliminado, sust->get_d(),decrece);
			if(decrece)
				elimina_re_balancea_dcha(sust,decrece);
		}
		else{
			eliminado -> set_dato(sust -> get_dato());
			eliminado = sust;
			sust = sust -> get_i();
			decrece = true;
		}
	}

	void print(){
		printR(raiz_);
	}
	void printR(nodoAVL<clave>* &nodo){
		if(nodo == NULL) return;
		printR(nodo->get_i());
		cout << nodo->get_dato();
		printR(nodo->get_d());
	}

	//Metodos getter y setter para los numeros de comparaciones
	inline int get_nCompB() const{
		return nCompB;
	}

	inline int get_nCompI() const{
		return nCompI;
	}

	inline void set_nCompB(const int& n){
		nCompB = n;
	}

	inline void set_nCompI(const int& n){
		nCompI = n;
	}

	void recorreN(){
		queue<nodoAVL<clave>*> Qd;	//Cola nodos
		queue<int> Qn;		//Cola niveles
		nodoAVL<clave>* nodo;
		int nivel, Nivel_actual=-1;

		Qd.push(raiz_);	Qn.push(0);

		while(!Qd.empty()){
			nodo = Qd.front();
			Qd.pop();
			nivel = Qn.front();
			Qn.pop();

			if(nivel > Nivel_actual){
				Nivel_actual = nivel;
				cout << endl << "L" << Nivel_actual << ":";
			}
			if(nodo != NULL){
				cout << "[" << nodo -> get_dato() << "]";

				Qd.push(nodo->get_i());	Qn.push(nivel+1);
				Qd.push(nodo->get_d());	Qn.push(nivel+1);
			}
			else
				cout << "[  NULL  ]";
		}

		cout << endl;
	}

	inline void Procesar(nodoAVL<clave>* nodo){
		cout << "[" << nodo -> get_dato() << "] ";
	}


};